#!/bin/bash
# Insname 
# <Mir Farshid Baha>
# <07.04.2015>
usage()
{
cat <<EOF
$0 [OPTIONS]
usage: insname [OPTIONS] FILE [FILE..]
Inserts the filename as the second line into
FILE, if FILE is a bash shell script
OPTIONS:
-v, --verbose
Display the name of each FILE being processed.
Display the result of each check for shell script

-h, --help 
Display this help

--version 
Display the version.
EOF
}
# ------------------------------------------------------------
version()
{
cat <<EOF
$0 [OPTIONS]
insname v1.0
@author: M.F. Baha
This is my first real try at shell scripting. Use at your own risk ;)
EOF
}
# ------------------------------------------------------------
verbose()
{
cat <<EOF
$0 [OPTIONS]

EOF
}
# ------------------------------------------------------------
filename()
{
echo "Please enter the FILE name(s):"
read filename
}
# ------------------------------------------------------------
execute()
{
filename
# grab the first two lines
first=$(head -1 "$filename")
second=$(head -2 "$filename" | grep -v "$first") 
 
if [[ "$first" == "#!/bin/bash" || "$first" == "#!/bin/sh" ]] && [[ "$second" == "" ]] ; then 
echo "$first" > tmpfile
echo "#$filename" >> tmpfile
tail -n +3 "$filename" >> tmpfile
mv tmpfile "$filename"
fi;
}
# ############################################################
# main
# check for options
## note the blanks after [ and before ]
if [ $# -lt 1 ]; then
# No option, so ask for name
execute
else
# at least 1 arg, lets check it
while [ "$1" != "" ]; do
case $1 in
"-h" | "--help") # the only valid arg
usage
exit
;;
"--version") # the only valid arg
version
exit
;;
"-v" | "--verbose") # the only valid arg
verbose
exit
;;
*) # anything else is not valid
echo "Invalid option"
exit 1
esac
shift
done
fi
exit 0